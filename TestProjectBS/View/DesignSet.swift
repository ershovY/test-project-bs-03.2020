//
//  ColorSet.swift
//  TestProjectBS
//
//  Created by Юрий Ершов on 05.03.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    static var designTextColor = UIColor.white
    static var designBackgroundColor = UIColor.black
    func designHeadline() -> UILabel {
        let label = UILabel()
        label.tintColor = UIColor.white
        label.shadowColor = UIColor.white.withAlphaComponent(0.5)
        label.font.withSize(14)
        return label
    }
    func designSubline() -> UILabel {
        let label = UILabel()
        label.tintColor = UIColor.white
        label.shadowColor = UIColor.white.withAlphaComponent(0.5)
        label.font.withSize(10)
        return label
    }
}

struct iconImage {
    var back = UIImage(systemName: "chevron.left")
    var more = UIImage(systemName: "ellipsis")
    var grid = UIImage(systemName: "rectangle.split.3x3")
    var account = UIImage(systemName: "person.crop.rectangle")
    var home = UIImage(systemName: "house")
    var loop = UIImage(systemName: "magnifyingglass")
    var add = UIImage(systemName: "plus.square")
    var hearth = UIImage(systemName: "suit.heart.fill")
    var round = UIImage(systemName: "person.circle")
    var checkmark = UIImage(systemName: "checkmark.circle")
}

extension UIViewController {
    func designVC() {
        self.view.backgroundColor = .black
        if let navController = navigationController {
            // background
            navController.navigationBar.isTranslucent = false
            navController.view.backgroundColor = .clear
            navController.navigationBar.barTintColor = .black
            // text
            navController.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        }
    }
}

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension UIColor {
    static var designTintColor = UIColor.white
}
