//
//  ViewController.swift
//  TestProjectBS
//
//  Created by Юрий Ершов on 05.03.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

final class FirstVC: UIViewController, UINavigationControllerDelegate {
    
    var setInfo: responseSet?
    
    // main stack
    lazy var mainStack = UIStackView()
    //bottom button bar
    lazy var bottomToolBar = UIStackView()
    lazy var home = UIButton()
    lazy var loop = UIButton()
    lazy var add = UIButton()
    lazy var hearth = UIButton()
    lazy var round = UIButton()
    // collectionRectBottoms
    lazy var BottomsStackForCollRect = UIStackView()
    lazy var grid = UIButton()
    lazy var account = UIButton()
    // collectionRect
    let collectionViewRectID = "Cell"
    var collectionRect: UICollectionView!
    let layoutRect:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    // collectionRound
    let collectionReuseableViewID = "reuseCellView"
    let supplementaryViewOfKindID = "Round"
    var collectionRound: UICollectionView!
    let layoutRound:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    
    // таргеты кнопок не создавал и ячейки коллекций можно было вынести отдельными классами.. - лимит времени(
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.designVC()
        self.setNavBar()
        //self.makeStackView()
        self.makeBottomToolBar()
        self.makeCollectionRect()
        self.makeButtonForCollRect()
        self.makeCollectionRound()
        
        Networking.shared.prepearedImages(string: "Cloud") { (value) in
            if value != nil {
                self.setInfo = value
                self.collectionRect.reloadData()
                self.collectionRound.reloadData()
            }
        }
    }
    
    
    //    MARK: - navigationController
    func setNavBar() {
        if navigationController != nil {
            // center
            self.navigationController?.navigationBar.topItem?.title = "earth"
            // right
            let more = UIBarButtonItem(image: iconImage().more, style: .done, target: self, action: #selector(moreAction))
            more.tintColor = .designTintColor
            self.navigationItem.rightBarButtonItem = more
            // left
            let back = UIBarButtonItem(image: iconImage().back, style: .done, target: self, action: #selector(backAction))
            back.tintColor = .designTintColor
            self.navigationItem.leftBarButtonItem = back
        }
    }
    @objc func moreAction () {}
    @objc func backAction () {}
    
    //    MARK: - mainStackView
    
    func makeStackView() {
        self.view.addSubview(mainStack)
        mainStack.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(400)
            //make.edges.equalToSuperview().inset(15)
        }
        mainStack.alignment = .fill
        mainStack.distribution = .fill
        mainStack.axis = .vertical
    }
    
    //    MARK: - BottomToolBar
    
    func makeBottomToolBar() {
        self.view.addSubview(bottomToolBar)
        bottomToolBar.alignment = .fill
        bottomToolBar.distribution = .fillEqually
        bottomToolBar.axis = .horizontal
        
        home.setImage(iconImage().home, for: .normal)
        home.tintColor = .designTintColor
        home.setTitleShadowColor(.designTintColor, for: .normal)
        loop.setImage(iconImage().loop, for: .normal)
        loop.setTitleShadowColor(.designTintColor, for: .normal)
        loop.tintColor = .designTintColor
        add.setImage(iconImage().add, for: .normal)
        add.setTitleShadowColor(.designTintColor, for: .normal)
        add.tintColor = .designTintColor
        hearth.setImage(iconImage().hearth, for: .normal)
        hearth.setTitleShadowColor(.designTintColor, for: .normal)
        hearth.tintColor = .designTintColor
        round.setImage(iconImage().round, for: .normal)
        round.setTitleShadowColor(.designTintColor, for: .normal)
        round.tintColor = .designTintColor
        
        bottomToolBar.addArrangedSubview(home)
        bottomToolBar.addArrangedSubview(loop)
        bottomToolBar.addArrangedSubview(add)
        bottomToolBar.addArrangedSubview(hearth)
        bottomToolBar.addArrangedSubview(round)
        bottomToolBar.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.trailing.leading.equalToSuperview()
            make.bottom.equalToSuperview().inset(10)
        }
    }
    
    //    MARK: - make Collection views
    func makeCollectionRect () {
        layoutRect.scrollDirection = .horizontal
        layoutRect.minimumLineSpacing = 1
        layoutRect.minimumInteritemSpacing = 1
        self.collectionRect = UICollectionView(frame: CGRect.zero, collectionViewLayout: layoutRect)
        self.collectionRect.indicatorStyle = .white
        self.collectionRect.backgroundColor = .clear
        self.view.addSubview(collectionRect)
        self.collectionRect.translatesAutoresizingMaskIntoConstraints = false
        collectionRect.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalTo(bottomToolBar.snp.top)
            make.height.equalTo((self.view.frame.height)/3)
        }
        
        self.collectionRect.tag = 100
        self.collectionRect.delegate = self
        self.collectionRect.dataSource = self
        self.collectionRect.register(UICollectionViewCell.self, forCellWithReuseIdentifier: collectionViewRectID)
        self.collectionRect.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: collectionViewRectID)
    }
    
    //    MARK: - make makeButtonForCollRect
    func makeButtonForCollRect () {
        self.view.addSubview(BottomsStackForCollRect)
        BottomsStackForCollRect.alignment = .fill
        BottomsStackForCollRect.distribution = .fillEqually
        BottomsStackForCollRect.axis = .horizontal
        BottomsStackForCollRect.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(collectionRect.snp.top).offset(5)
            make.height.equalTo(50)
        }
        grid.setImage(iconImage().grid, for: .normal)
        grid.tintColor = .designTintColor
        grid.setTitleShadowColor(.designTintColor, for: .normal)
        account.setImage(iconImage().account, for: .normal)
        account.tintColor = .designTintColor
        account.setTitleShadowColor(.designTintColor, for: .normal)
        BottomsStackForCollRect.addArrangedSubview(self.grid)
        BottomsStackForCollRect.addArrangedSubview(self.account)
    }
    
    //    MARK: - make makeCollectionRound
    func makeCollectionRound () {
        self.collectionRound = UICollectionView(frame: CGRect.zero, collectionViewLayout: layoutRound)
        self.view.addSubview(collectionRound)
        self.collectionRound.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: collectionReuseableViewID)
        layoutRound.scrollDirection = .horizontal
        layoutRound.minimumLineSpacing = 2
        layoutRound.minimumInteritemSpacing = 1
        self.collectionRound.indicatorStyle = .white
        self.collectionRound.backgroundColor = .clear
        self.collectionRound.translatesAutoresizingMaskIntoConstraints = false
        collectionRound.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(150)
            make.bottom.equalTo(BottomsStackForCollRect.snp.top)
        }
        
        self.collectionRound.tag = 200
        self.collectionRound.delegate = self
        self.collectionRound.dataSource = self
    }
}

extension FirstVC: UICollectionViewDelegate {
    
}

// MARK: - UICollectionViewDataSource
extension FirstVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 100,200:
            if self.setInfo != nil {
                return setInfo!.resultCount!
            } else {
                return 30 }
        default:
            assert(false, "📛 Invalid tag")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        print("go")
        //        guard kind == self.supplementaryViewOfKindID, collectionView.tag == 200 else {assert(false, "📛 Invalid element type")}
        let cell = collectionView.dequeueReusableSupplementaryView(ofKind: supplementaryViewOfKindID, withReuseIdentifier: self.collectionReuseableViewID, for: indexPath)
        let imageView = UIImageView()
        let stack = UIStackView()
        let label = UILabel()
        label.text = self.setInfo!.results[indexPath.row].artistName!
        
        stack.axis = .vertical
        stack.distribution = .fill
        
        label.textColor = .designTintColor
        label.textAlignment = .center
        stack.addArrangedSubview(imageView)
        stack.addArrangedSubview(label)
        cell.addSubview(stack)
        imageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(100)
        }
        label.snp.makeConstraints { (make) in
            make.leading.trailing.equalTo(stack)
        }
        stack.snp.makeConstraints { (make) in
            make.leading.trailing.height.bottom.equalTo(cell)
        }
        imageView.layer.cornerRadius = 100 / 2
        imageView.clipsToBounds = true
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.gray.cgColor
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.sd_setImage(with: URL(string: self.setInfo!.results[indexPath.row].artworkUrl100!), completed: nil)
        imageView.contentMode = .scaleAspectFill
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard collectionView.tag == 100 else { assert(false, "❌ cell with tag \(collectionView.tag) don't call")}
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewRectID, for: indexPath)
            if self.setInfo != nil {
                let imageView = UIImageView()
                cell.clipsToBounds = true
                cell.addSubview(imageView)
                imageView.snp.makeConstraints { (make) in
                    make.leading.trailing.height.bottom.equalTo(cell)
                }
                imageView.translatesAutoresizingMaskIntoConstraints = false
                imageView.sd_setImage(with: URL(string: self.setInfo!.results[indexPath.row].artworkUrl100!), completed: nil)
                imageView.contentMode = .scaleAspectFill
                return cell } else {
                return cell
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView.tag {
        case 100:
            return CGSize(width: self.view.frame.height/3/2, height: self.view.frame.height/3/2)
        case 200:
            return CGSize(width: 100, height: 150)
        default:
            assert(false, "📛 Invalid tag")
        }
    }
}
