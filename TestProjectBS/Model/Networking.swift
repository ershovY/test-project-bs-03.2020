//
//  Networking.swift
//  TestProjectBS
//
//  Created by Юрий Ершов on 06.03.2020.
//  Copyright © 2020 smartProject. All rights reserved.
//

import Foundation
import Alamofire

final class Networking {
    
    static let shared = Networking()
    
    enum pathes: String {
        case search = "https://itunes.apple.com/search"
    }
    
    func prepearedImages(string something: String, complition: @escaping (responseSet?)-> Void) {
        AF.request(pathes.search.rawValue, method: .get, parameters: ["term":something, "media": "music", "limit": "50"])
        .validate(statusCode: 200..<300)
        .responseData { response in
            switch response.result {
            case .success:
                do
                {
                    let info: responseSet = try JSONDecoder().decode(responseSet.self, from: response.value!)
                        complition(info)
                }
                catch
                {
                    print("❌ - \(error.localizedDescription)")
                    debugPrint(error)
                }
            case let .failure(error):
                print("❌\(error)")
            }
        }
        
    }

}

struct responseSet: Decodable {
    var resultCount: Int?
    var results: [infoSet]
    
    struct infoSet: Decodable {
        var artworkUrl100: String?
        var artistName: String?
    }
}
